# Easy Umbraco Forms (EUF)

Written by Chris Day (@daymedia) and Chris Dixon (@pxlcode)

Based on the original script by Chris Dixon here:
http://pixeltocode.uk/blog/umbraco/easy-umbraco-forms/

## Introduction
EUF is a simple Umbraco package that will convert a document type of your choosing into a contact form.

When a user completes the form, it will email you and save the details to an Umbraco node stored in a given folder.

This is designed to be a very basic, free solution. If your requirements are more advanced, we recommend checking out Umbraco Forms:
http://umbraco.com/products-and-support/forms/

## How to use it
(needs more work)
1. Drop EasyUmbracoForms.dll into your bin folder.
2. Copy the sample view ```ContactForm.cshtml``` to your ```Views/Partials``` folder. Customise to your hearts content.
3. Create a document type for your contact form, try adding fields for name, email, and message.
4. Create a folder to store the contact form submissions
5. Add the following four properties to any document type that is going to use a template containing the form:
    - formDocumentType (Textstring), The document type we are generating our form from
    - storageFolder (Content Picker), The folder that form submissions will be stored in
    - fromEmailAddress (Textstring)
    - toEmailAddress (Textstring)
    - thankYouMessage (Richtext editor)
    - thankYouPage (Content Picker), [Optional] select a separate Umbraco page to redirect to on success (overrides thankYouMessage)
6. Add values to the four properties above on your chosen node.
7. Add the form to your template:
    ```@Html.Action("Index", "EUFContactFormSurface", new { pageId = CurrentPage.Id, cfvm = new EUFContactFormViewModel(CurrentPage) })```