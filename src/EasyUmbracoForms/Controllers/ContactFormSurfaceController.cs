﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;

using EasyUmbracoForms.Models;
using EasyUmbracoForms.Services;

namespace EasyUmbracoForms.Controllers
{
    public class EUFContactFormSurfaceController : SurfaceController
    {
		public ActionResult Index(int pageId, EUFContactFormViewModel cfvm = default(EUFContactFormViewModel))
		{
			if (cfvm == default(EUFContactFormViewModel))
			{
				cfvm = new EUFContactFormViewModel(Umbraco.TypedContent(pageId));
			}

			return PartialView("ContactForm", cfvm);
		}

        [HttpPost]
        public ActionResult Index(EUFContactFormViewModel cfvm)
        {
            // Pass the form post to the model
            cfvm.FormPost = Request;
            TryUpdateModel<EUFContactFormViewModel>(cfvm);

            if (ModelState.IsValid)
            {
                cfvm.SaveAndSendMessage(Services.ContentService);
                TempData["ContactFormSuccess"] = true;

                // We have a choice of displaying a message on the same page or redirecting
                if (cfvm.ThankyouPage > 0)
                {
                    var ty = Umbraco.TypedContent(cfvm.ThankyouPage);
                    Response.Redirect(ty.Url);
                }
                
                return PartialView("ContactForm", cfvm);
            }
            else
            {
                TempData["ContactFormSuccess"] = null;
                return PartialView("ContactForm", cfvm);
            }
        }
    }
}
