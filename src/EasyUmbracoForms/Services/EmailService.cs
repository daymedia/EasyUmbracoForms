﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;
using System.Configuration;

namespace EasyUmbracoForms.Services
{
    public class EmailService
    {
        // Single recipient
        public void SendEmail(string html, string subject, string emailTo, string emailFrom)
        {
            using (var smtp = new SmtpClient())
            using (var message = BuildEmail(html, subject, emailTo, emailFrom))
            {
                smtp.Send(message);
            }
        }


        // Multiple recipients
        public void SendEmail(string html, string subject, List<string> emailsTo, string emailFrom)
        {
            using (var smtp = new SmtpClient())
            {
                foreach (var email in emailsTo)
                {
                    using (var message = BuildEmail(html, subject, email, emailFrom))
                    {
                        smtp.Send(message);
                    }
                }
            }
        }

        private MailMessage BuildEmail(string html, string subject, string emailTo, string emailFrom)
        {
            MailMessage message = new MailMessage(emailFrom, emailTo, subject, html);

            message.IsBodyHtml = true;

            return message;
        }

        public void SendEmail(List<CustomField> customFields, string html, string subject, string emailTo, string emailFrom)
        {
            using (var smtp = new SmtpClient())
            using (var message = BuildEmail(customFields, html, subject, emailTo, emailFrom))
            {
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                smtp.UseDefaultCredentials = true;
                smtp.Host = smtpSection.Network.Host;
                smtp.Credentials = new System.Net.NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                smtp.Send(message);
            }
        }

        private MailMessage BuildEmail(List<CustomField> customFields, string html, string subject, string emailTo, string emailFrom)
        {
            foreach (CustomField field in customFields)
                html = field.Replace(html);

            MailMessage message = new MailMessage(emailFrom, emailTo, subject, html);

            message.IsBodyHtml = true;

            return message;
        }
    }

    public class CustomField
    {
        private string _name;
        private string _value;

        public string Name { get; set; }
        public string Value { get; set; }

        public CustomField(string name, string value)
        {
            _name = name;
            _value = value;
        }

        public string Replace(string html)
        {
            return html.Replace(_name, _value);
        }
    }
}