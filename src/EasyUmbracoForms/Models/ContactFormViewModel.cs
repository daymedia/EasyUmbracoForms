﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Text.RegularExpressions;

using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

using EasyUmbracoForms.Services;


namespace EasyUmbracoForms.Models
{
    public class EUFContactFormViewModel : PublishedContentWrapped, IValidatableObject
    {
        public EUFContactFormViewModel(IPublishedContent content) : base(content)
	    {
            CurrentPage = content;

			FromEmailAdddress = CurrentPage.GetPropertyValue<string>("fromEmailAddress");
			ToEmailAdddress = CurrentPage.GetPropertyValue<string>("toEmailAddress");
			ThankyouPage = CurrentPage.GetPropertyValue<int>("thankYouPage");
		}

        public IPublishedContent CurrentPage { get; set; }

        public int FolderId { get; set; }

        public bool Success { get; set; }

        private IContentType _formDocType;

        public IContentType FormDocType
        {
            get
            {
                // Initialise the document type
                if (_formDocType == null)
                {
                    // name of the doctype we are using to build and store this form
                    string doctype = CurrentPage.GetPropertyValue<string>("formDocumentType");

					// folder in content tree where form submissions will be stored
					int tmp;
                    if(int.TryParse(CurrentPage.GetPropertyValue<string>("StorageFolder"), out tmp))
					{
						FolderId = tmp;
					}

                    // Umbraco content API - get the content type of the form
                    IContentService contentService = ApplicationContext.Current.Services.ContentService;
                    IContentTypeService contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
                    _formDocType = contentTypeService.GetContentType(doctype);
                }

                return _formDocType;
            }

			set
			{
				_formDocType = value;
			}
        }

        public string FromEmailAdddress { get; set; }

        public string ToEmailAdddress { get; set; }

        public int ThankyouPage { get; set; }

        // The entire Http form post is passed in to the model, as we cannot map
        // the dynamic fields to the model itself
        public HttpRequestBase FormPost { get; set; }

        // Custom Validation, we can't use the standard []'s because
        // the properties are stored in the doc type and not part of the model
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // loop through properties and validate input
            foreach (PropertyGroup pg in FormDocType.PropertyGroups)
            {
                foreach (PropertyType pt in pg.PropertyTypes.OrderBy(x => x.SortOrder))
                {
                    var fieldName = String.Format("{0}{1}", pt.Alias, pt.Id);

                    if (pt.Mandatory && string.IsNullOrEmpty(FormPost.Form[fieldName]))
                    {
                        yield return new ValidationResult(String.Format("{0} cannot be left empty.", pt.Name));
                    }
                    if (!string.IsNullOrEmpty(pt.ValidationRegExp))
                    {
                        Regex regex = new Regex(pt.ValidationRegExp, RegexOptions.IgnoreCase);

                        if (!regex.IsMatch(FormPost.Form[fieldName].ToString()))
                        {
                            yield return new ValidationResult(String.Format("{0} is not a valid.", pt.Name));
                        }
                    }
                }
            }

            // Todo: Get this working again
            // check the anti-forgery token matches
            //try
            //{
            //    AntiForgery.Validate(Request.Cookies["__RequestVerificationToken"].Value, Request.Form["__RequestVerificationToken"].ToString());
            //}
            //catch
            //{
            //    yield return new ValidationResult("Anti forgery token doesn't match.");
            //}
        }

        public void SaveAndSendMessage(IContentService contentService)
        {
            SaveMessage(contentService);
            if (ToEmailAdddress.IsNullOrWhiteSpace() == false || ToEmailAdddress != string.Empty)
            {
                SendEmail();
            }
            Success = true;
        }

        private const string SUBJECT_HEADING = "Website contact form sent at {0}";
        private const string SPAM_HEADING = "[SPAM] Website form sent at {0}";

        public void SaveMessage(IContentService contentService)
        {
            // Check the honeypot and mark spam messages
            string subjectHeading;

            if (Url != null)
                subjectHeading = String.Format(SPAM_HEADING, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            else
                subjectHeading = String.Format(SUBJECT_HEADING, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

			if (FolderId > 0)
			{
				var ct = contentService.CreateContent(subjectHeading, FolderId, FormDocType.Alias, 0);

				foreach (PropertyType pt in FormDocType.PropertyTypes)
				{
					var fieldName = pt.Alias + pt.Id;
					var fieldValue = FormPost.Form[fieldName];
					ct.SetValue(pt.Alias, fieldValue);
				}

				contentService.Save(ct);
			}
        }

        public string Url { get; set; }

        public void SendEmail()
        {
			if (String.IsNullOrWhiteSpace(ToEmailAdddress))
			{
				return;
			}

            string subjectHeading;

            // Check the honeypot and mark spam messages
            if (Url != null)
                subjectHeading = String.Format(SPAM_HEADING, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            else
                subjectHeading = String.Format(SUBJECT_HEADING, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            // Send the email
            EmailService es = new EmailService();

            if(ToEmailAdddress.Contains(","))
            {
                // Multiple Recipients
                List<string> toEmails = ToEmailAdddress.Split(',').ToList();
                es.SendEmail(BuildHtml(), subjectHeading, toEmails, FromEmailAdddress);
            }
            else
            {
                // Single Recipient
                es.SendEmail(BuildHtml(), subjectHeading, ToEmailAdddress, FromEmailAdddress);
            }
        }

        public string BuildHtml()
        {
            StringBuilder sb = new StringBuilder("<h1>Website Contact Form</h1>");

            foreach (PropertyType pt in FormDocType.PropertyTypes)
            {
                var fieldName = pt.Alias + pt.Id;
                var fieldValue = formatTextArea(FormPost.Form[fieldName].ToString());

                sb.Append("<h2>");
                sb.Append(pt.Name);
                sb.Append("</h2><p>");
                sb.Append(fieldValue);
                sb.Append("</p>");
            }

            return sb.ToString();
        }

        private string formatTextArea(string input)
        {
            string output = HttpUtility.HtmlEncode(input);
            return output.Replace(System.Environment.NewLine, "<br />");
        }
    }
}